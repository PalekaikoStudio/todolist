//Clicking the Span (Trashcan) fades and then deletes the item.
$("ul").on("click", "span", function(event){
    $(this).parent().fadeOut(500,function(){
        $(this).remove();
    });
    event.stopPropagation();
});

// Clicking on a To-do will toggle strikethrough and grey out.
$("ul").on("click", "li", function(){
    $(this).toggleClass("completedTodo");
});

//When the user presses "Enter" in the text input, it will append that text as an item to the list.
$("input[type='text']").keypress(function(event){
    if(event.which === 13 ) {
        if ($(this).val().length > 0) {
            var todoText = $(this).val();
            $(this).val("");
            $("ul").append("<li><span><i class='fas fa-trash'></i></span>" + todoText + "</li>")
        }
    }
});
