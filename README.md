# TodoList

A simple to-do list used as a proof of concept.

Play with it here!

https://codepen.io/palekaikostudios/pen/KJYobM

The To-Do supports adding, removing and checking off Items. 

Using simple logic to keep the user from adding blank entries and otherwise works as a simple
list.

The animated gradient background is thanks to Manuel Pinto. The original
Code Pen can be found here https://codepen.io/P1N2O/pen/pyBNzX

A few improvments down the road would be some bug fixes. Biggest thing is 
sanitizing user input, try adding a todo with something like

`<li> Item </li>`

I'd also like to add user accounts, saving lists, adding metrics for items
completed and added and intergrating it with GSuite for reminders and 
overviews of to-dos.

But for now, it's a simple idea that I'll come back to.